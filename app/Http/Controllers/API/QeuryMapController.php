<?php 

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class QeuryMapController extends Controller
{
    public function call_map(Request $req){

        // dd($req->all());
        $return_data = [];
        $return_data['datas'] = [];
        $return_data['status'] = false;
        $return_data['msg'] = "";
        try {
            
            $text_search = $req->input('text_search');
            $API_KEY_MAP = env('API_KEY_MAP', null);
            if(empty($text_search)){
                $return_data['msg'] = "empty string text_search!";
            }else if(empty($API_KEY_MAP)){
                $return_data['msg'] = "empty string KEY_MAP!";
            }else{

                // $json = json_decode(file_get_contents(storage_path('test_muck.json')), true); // ดึงค่าจากไฟล์ที่ muck data (ตัวอย่าง)
                // $return_data['status'] = true;
                // $return_data['datas'] = $json['results'];

                $end_point = "https://maps.googleapis.com/maps/api/place/textsearch/json";
                $arr_search = [
                    'key' => $API_KEY_MAP, //KEY API
                    'query' => trim($text_search), //ค้นหาด้วยข้อความ
                    'types' => "restaurant|food", //ประเภทอาหาร
                    'language' => "th" // ภาษา
                ];
                $res = Http::get($end_point,$arr_search)->json(); // call request api to google map api 
                $return_data['status'] = true;
                if($res['status'] == 'OK'){ // เซ็ตสถานะข้อมูล
                    $return_data['datas'] = $res['results'];
                }

            }
            return response()->json($return_data,200);

        } catch (\Exception $error) {
            $getMessage = $error->getMessage();
            Log::error($getMessage);
            $return_data['msg'] = $getMessage;
            return response()->json($return_data,500);
        }

    }
}

?>