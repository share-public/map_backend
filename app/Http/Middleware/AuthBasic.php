<?php 

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class AuthBasic {

    public function handle(Request $request, Closure $next)
    {
        $key_user = env('BASIC_USER', null);
        $key_pass = env('BASIC_PASS', null);
        $user = $request->getUser();
        $pass = $request->getPassword();
        $return_data = [];
        $return_data['status'] = false;
        $return_data['msg'] = "";
        try {
            if($user == $key_user && $pass == $key_pass){ // เช็ตข้อมูล header request ว่า username และ password ถูกต้องหรือไม่
                return $next($request);
            }else{
                Log::debug('User Or Password wrong');
                $return_data['msg'] = 'User Or Password wrong';
                return response()->json($return_data,401);
            }
        } catch (\Exception $error) {
            Log::error($error->getMessage());
            $return_data['msg'] = 'Unauthorized action.';
            return response()->json($return_data,401);
        }
    }
    
}


?>